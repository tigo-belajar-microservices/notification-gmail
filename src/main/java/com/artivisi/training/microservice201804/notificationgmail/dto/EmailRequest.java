package com.artivisi.training.microservice201804.notificationgmail.dto;

import lombok.Data;

@Data
public class EmailRequest {
    private String to;
    private String from;
    private String subject;
    private String content;
}
